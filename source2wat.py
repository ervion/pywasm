import ast


class Namespace:
    def __init__(self, start):
        self._mem = {}
        self.end = start

    def get(self, identifier):
        if identifier not in self._mem:
            self._mem[identifier] = self.end
            self.end += 4
        return self._mem[identifier]

    def get_or_none(self, identifier):
        if identifier not in self._mem:
            return None
        return self._mem[identifier]

    def set(self, identifier, address):
        self._mem[identifier] = address


class Program:
    def __init__(self, py_source: str = None):
        self.wat = ''  # WebAssembly text format
        self._functions = []
        if py_source is not None:
            self._walk(ast.parse(py_source), [Namespace(4)], 0, 0)

    def _visit(self, *args) -> None:
        self.wat += '    ' + ' '.join(map(str, args)) + '\n'

    def _walk(self, node: ast.AST, namespaces: [Namespace], loop_depth: int, call_depth: int) -> None:
        # module
        if isinstance(node, ast.Module):
            self.wat += '(module\n'
            self.wat += '  (import "stdlib" "print" (func $print (param i32) (result i32)))\n'
            self.wat += '  (memory 1)\n'
            self.wat += '  (func (export "main")\n'
            for child in node.body:
                self._walk(child, namespaces, loop_depth, call_depth)
            self.wat += '  )\n'
            for f in self._functions:
                self.wat += f
            self.wat += ')\n'
        elif isinstance(node, ast.Interactive):
            self._visit('unreachable ;; Interactive')
        elif isinstance(node, ast.Expression):
            self._visit('unreachable ;; Expression')
        elif isinstance(node, ast.Suite):
            for child in node.body:
                self._walk(child, namespaces, loop_depth, call_depth)
        # statement
        elif isinstance(node, ast.FunctionDef):
            if node.returns is not None:
                raise NotImplementedError("Only int32 return type supported")
            if len(node.decorator_list) != 0:
                raise NotImplementedError("Function decorators not supported")
            comp = Program()
            comp.wat += "  (func $" + node.name
            for argument in node.args.args:
                comp.wat += " (param $" + argument.arg + " i32)"
            comp.wat += " (result i32)\n"
            comp._visit('block i32')
            inner_namespaces = namespaces[:]
            inner_namespaces.append(Namespace(namespaces[-1].end))
            for argument in node.args.args:
                comp._visit('i32.const', inner_namespaces[-1].get(argument.arg))
                comp._visit('get_local', '$' + argument.arg)
                comp._visit('i32.store')
            for stmt in node.body:
                comp._walk(stmt, inner_namespaces, 0, 0)
            comp._visit('i32.const 0')
            comp._visit('end')
            comp.wat += "  )\n"
            self._functions.append(comp.wat)
        elif isinstance(node, ast.AsyncFunctionDef):
            self._visit('unreachable ;; AsyncFunctionDef')
        elif isinstance(node, ast.ClassDef):
            self._visit('unreachable ;; ClassDef')
        elif isinstance(node, ast.Return):
            self._walk(node.value, namespaces, loop_depth, call_depth)
            self._visit('br', call_depth, ';; Return')
        elif isinstance(node, ast.Delete):
            self._visit('unreachable ;; Delete')
        elif isinstance(node, ast.Assign):
            self._visit('i32.const 0')
            self._walk(node.value, namespaces, loop_depth, call_depth)
            self._visit('i32.store')
            for target in node.targets:
                self._walk(target, namespaces, loop_depth, call_depth)
                self._visit('i32.const 0')
                self._visit('i32.load')
                self._visit('i32.store')
        elif isinstance(node, ast.AugAssign):
            operators = {
                ast.Add: 'i32.add',
                ast.Sub: 'i32.sub',
                ast.Mult: 'i32.mul',
                ast.MatMult: 'unreachable ;; MatMul',
                ast.Div: 'unreachable ;; Div',
                ast.FloorDiv: 'i32.div_s',
                ast.Mod: 'i32.rem_s',
                ast.BitAnd: 'i32.and',
                ast.BitOr: 'i32.or',
                ast.BitXor: 'i32.xor',
                ast.LShift: 'i32.shl',
                ast.RShift: 'i32.shr_s'
            }
            self._walk(node.target, namespaces, loop_depth, call_depth)
            self._walk(node.target, namespaces, loop_depth, call_depth)
            self._visit('i32.load')
            self._walk(node.value, namespaces, loop_depth, call_depth)
            self._visit(operators[type(node.op)])
            self._visit('i32.store')
        # elif isinstance(node, ast.AnnAssign):
        #     self._visit('unreachable ;; AnnAssign')
        elif isinstance(node, ast.For):
            self._visit('unreachable ;; For')
        elif isinstance(node, ast.AsyncFor):
            self._visit('unreachable ;; AsyncFor')
        elif isinstance(node, ast.While):
            self._visit('block')
            call_depth += 1
            self._visit('loop')
            call_depth += 1
            loop_depth = 0
            self._walk(node.test, namespaces, loop_depth, call_depth)
            self._visit('i32.eqz')
            self._visit('if')
            loop_depth += 1
            call_depth += 1
            for stmt in node.orelse:
                self._walk(stmt, namespaces, loop_depth, call_depth)
            self._visit('br', loop_depth + 1, ';; Break Loop')
            self._visit('end')  # if
            loop_depth -= 1
            call_depth -= 1
            for stmt in node.body:
                self._walk(stmt, namespaces, loop_depth, call_depth)
            self._visit('br', loop_depth, ';; Loop')
            self._visit('end')  # loop
            call_depth -= 1
            self._visit('end')  # block
            call_depth -= 1
        elif isinstance(node, ast.If):
            self._walk(node.test, namespaces, loop_depth, call_depth)
            self._visit('if')
            loop_depth += 1
            call_depth += 1
            for stmt in node.body:
                self._walk(stmt, namespaces, loop_depth, call_depth)
            self._visit('else')
            for stmt in node.orelse:
                self._walk(stmt, namespaces, loop_depth, call_depth)
            self._visit('end')
            loop_depth -= 1
            call_depth -= 1
        elif isinstance(node, ast.With):
            self._visit('unreachable ;; With')
        elif isinstance(node, ast.AsyncWith):
            self._visit('unreachable ;; AsyncWith')
        elif isinstance(node, ast.Raise):
            self._visit('unreachable ;; Raise')
        elif isinstance(node, ast.Try):
            self._visit('unreachable ;; Try')
        elif isinstance(node, ast.Assert):
            self._walk(node.test, namespaces, loop_depth, call_depth)
            self._visit('i32.eqz')
            self._visit('if')
            loop_depth += 1
            call_depth += 1
            if node.msg is not None:
                self._walk(node.msg, namespaces, loop_depth, call_depth)
                self._visit('call $print')
            self._visit('unreachable ;; AssertionError')
            self._visit('end')
            loop_depth -= 1
            call_depth -= 1
        elif isinstance(node, ast.Import):
            self._visit('unreachable ;; Import')
        elif isinstance(node, ast.ImportFrom):
            self._visit('unreachable ;; ImportFrom')
        elif isinstance(node, ast.Global):
            for name in node.names:
                if namespaces[0].get_or_none(name):
                    namespaces[-1].set(name, namespaces[0].get_or_none(name))
        elif isinstance(node, ast.Nonlocal):
            for name in node.names:
                if namespaces[-2].get_or_none(name):
                    namespaces[-1].set(name, namespaces[-2].get_or_none(name))
        elif isinstance(node, ast.Expr):
            self._walk(node.value, namespaces, loop_depth, call_depth)
            self._visit('drop')
        elif isinstance(node, ast.Pass):
            self._visit('nop')
        elif isinstance(node, ast.Break):
            self._visit('br', loop_depth + 1, ';; Break')
        elif isinstance(node, ast.Continue):
            self._visit('br', loop_depth, ';; Continue')
        # expression
        elif isinstance(node, ast.BoolOp):
            if type(node.op) == ast.And:
                def and_walker(depth, and_loop_depth, and_call_depth):
                    if depth == len(node.values) - 1:
                        self._visit('i32.const 0')
                        self._visit('i32.load')
                        return
                    # load left arg
                    self._visit('i32.const 0')
                    self._visit('i32.load')
                    self._visit('i32.eqz')
                    self._visit('i32.eqz')
                    # load right arg and store
                    self._visit('i32.const 0')
                    self._walk(node.values[depth + 1], namespaces, and_loop_depth, and_call_depth)
                    self._visit('i32.store')
                    self._visit('i32.const 0')
                    self._visit('i32.load')
                    self._visit('i32.eqz')
                    self._visit('i32.eqz')

                    self._visit('i32.and')
                    self._visit('if i32')
                    and_loop_depth += 1
                    and_call_depth += 1
                    and_walker(depth + 1, and_loop_depth, and_call_depth)
                    self._visit('else')
                    self._visit('i32.const 0')
                    self._visit('end')
                    and_loop_depth -= 1
                    and_call_depth -= 1

                self._visit('i32.const 0')
                self._walk(node.values[0], namespaces, loop_depth, call_depth)
                self._visit('i32.store')
                self._visit('i32.const 0')
                self._visit('i32.load')

                self._visit('if i32')
                loop_depth += 1
                call_depth += 1
                and_walker(0, loop_depth, call_depth)
                self._visit('else')
                self._visit('i32.const 0')
                self._visit('end')
                loop_depth -= 1
                call_depth -= 1
            elif type(node.op) == ast.Or:
                def or_walker(depth, or_loop_depth, or_call_depth):
                    if depth == len(node.values) - 1:
                        self._visit('i32.const 0')
                        return
                    # load left arg
                    self._visit('i32.const 0')
                    self._visit('i32.load')
                    self._visit('i32.eqz')
                    self._visit('i32.eqz')
                    # load right arg and store
                    self._visit('i32.const 0')
                    self._walk(node.values[depth + 1], namespaces, or_loop_depth, or_call_depth)
                    self._visit('i32.store')
                    self._visit('i32.const 0')
                    self._visit('i32.load')
                    self._visit('i32.eqz')
                    self._visit('i32.eqz')

                    self._visit('i32.or')
                    self._visit('if i32')
                    or_loop_depth += 1
                    or_call_depth += 1
                    self._visit('i32.const 0')
                    self._visit('i32.load')
                    self._visit('else')
                    or_walker(depth + 1, or_loop_depth, or_call_depth)
                    self._visit('end')
                    or_loop_depth -= 1
                    or_call_depth -= 1

                self._visit('i32.const 0')
                self._walk(node.values[0], namespaces, loop_depth, call_depth)
                self._visit('i32.store')
                self._visit('i32.const 0')
                self._visit('i32.load')

                self._visit('if i32')
                loop_depth += 1
                call_depth += 1
                self._visit('i32.const 0')
                self._visit('i32.load')
                self._visit('else')
                or_walker(0, loop_depth, call_depth)
                self._visit('end')
                loop_depth -= 1
                call_depth -= 1
        elif isinstance(node, ast.BinOp):
            self._walk(node.left, namespaces, loop_depth, call_depth)
            self._walk(node.right, namespaces, loop_depth, call_depth)
            operators = {
                ast.Add: 'i32.add',
                ast.Sub: 'i32.sub',
                ast.Mult: 'i32.mul',
                ast.MatMult: 'unreachable ;; MatMul',
                ast.Div: 'unreachable ;; Div',
                ast.FloorDiv: 'i32.div_s',
                ast.Mod: 'i32.rem_s',
                ast.BitAnd: 'i32.and',
                ast.BitOr: 'i32.or',
                ast.BitXor: 'i32.xor',
                ast.LShift: 'i32.shl',
                ast.RShift: 'i32.shr_s'
            }
            self._visit(operators[type(node.op)])
        elif isinstance(node, ast.UnaryOp):
            if isinstance(node.op, ast.Invert):
                self._walk(node.operand, namespaces, loop_depth, call_depth)
                self._visit('i32.const -1')
                self._visit('i32.xor')
            elif isinstance(node.op, ast.Not):
                self._walk(node.operand, namespaces, loop_depth, call_depth)
                self._visit('i32.eqz')
            elif isinstance(node.op, ast.UAdd):
                self._walk(node.operand, namespaces, loop_depth, call_depth)
            elif isinstance(node.op, ast.USub):
                self._visit('i32.const 0')
                self._walk(node.operand, namespaces, loop_depth, call_depth)
                self._visit('i32.sub')
        elif isinstance(node, ast.Lambda):
            self._visit('unreachable ;; Lambda')
        elif isinstance(node, ast.IfExp):
            self._walk(node.test, namespaces, loop_depth, call_depth)
            self._visit('if i32')
            loop_depth += 1
            call_depth += 1
            self._walk(node.body, namespaces, loop_depth, call_depth)
            self._visit('else')
            self._walk(node.orelse, namespaces, loop_depth, call_depth)
            self._visit('end')
            loop_depth -= 1
            call_depth -= 1
        elif isinstance(node, ast.Dict):
            self._visit('unreachable ;; Dict')
        elif isinstance(node, ast.Set):
            self._visit('unreachable ;; Set')
        elif isinstance(node, ast.ListComp):
            self._visit('unreachable ;; ListComp')
        elif isinstance(node, ast.SetComp):
            self._visit('unreachable ;; SetComp')
        elif isinstance(node, ast.DictComp):
            self._visit('unreachable ;; DictComp')
        elif isinstance(node, ast.GeneratorExp):
            self._visit('unreachable ;; GeneratorExp')
        elif isinstance(node, ast.Await):
            self._visit('unreachable ;; Await')
        elif isinstance(node, ast.Yield):
            self._visit('unreachable ;; Yield')
        elif isinstance(node, ast.YieldFrom):
            self._visit('unreachable ;; YieldFrom')
        elif isinstance(node, ast.Compare):
            operands = [node.left]
            operands.extend(node.comparators)
            operators = {
                ast.Eq: 'i32.eq',
                ast.NotEq: 'i32.ne',
                ast.Lt: 'i32.lt_s',
                ast.LtE: 'i32.le_s',
                ast.Gt: 'i32.gt_s',
                ast.GtE: 'i32.ge_s',
                ast.Is: 'i32.eq ;; Is',
                ast.IsNot: 'i32.ne ;; IsNot',
                ast.In: 'unreachable ;; In',
                ast.NotIn: 'unreachable ;; NotIn'
            }
            self._visit('i32.const', 1)
            self._visit('i32.const 0')
            self._walk(operands[0], namespaces, loop_depth, call_depth)
            self._visit('i32.store')
            for i in range(1, len(operands)):
                self._visit('i32.const 0')
                self._visit('i32.load')
                self._visit('i32.const 0')
                self._walk(operands[i], namespaces, loop_depth, call_depth)
                self._visit('i32.store')
                self._visit('i32.const 0')
                self._visit('i32.load')
                self._visit(operators[type(node.ops[i - 1])])
                self._visit('i32.and')
        elif isinstance(node, ast.Call):
            for arg in node.args:
                self._walk(arg, namespaces, loop_depth, call_depth)
            self._visit('call', '$' + node.func.id)
        elif isinstance(node, ast.Num):
            self._visit('i32.const', node.n)
        elif isinstance(node, ast.Str):
            self._visit('unreachable ;; Str')
        # elif isinstance(node, ast.FormattedValue):
        #     self._visit('unreachable ;; FormattedValue')
        # elif isinstance(node, ast.JoinedStr):
        #     self._visit('unreachable ;; JoinedStr')
        elif isinstance(node, ast.Bytes):
            self._visit('unreachable ;; Bytes')
        elif isinstance(node, ast.NameConstant):
            if node.value:
                self._visit('i32.const 1 ;; NameConstant ' + str(node.value))
            else:
                self._visit('i32.const 0 ;; NameConstant ' + str(node.value))
        elif isinstance(node, ast.Ellipsis):
            self._visit('unreachable ;; Ellipsis')
        # elif isinstance(node, ast.Constant):
        #     self._visit('unreachable ;; Constant')
        elif isinstance(node, ast.Attribute):
            self._visit('unreachable ;; Attribute')
        elif isinstance(node, ast.Subscript):
            self._visit('unreachable ;; Subscript')
        elif isinstance(node, ast.Starred):
            self._visit('unreachable ;; Starred')
        elif isinstance(node, ast.Name):
            if isinstance(node.ctx, ast.Store):
                self._visit('i32.const ' + str(namespaces[-1].get(node.id)) + ' ;; ' + str(node.id))
            elif isinstance(node.ctx, ast.Load):
                self._visit('i32.const ' + str(namespaces[-1].get(node.id)) + ' ;; ' + str(node.id))
                self._visit('i32.load')
            elif isinstance(node.ctx, ast.Del):
                self._visit('unreachable ;; Del')
            elif isinstance(node.ctx, ast.AugLoad):
                self._visit('unreachable ;; AugLoad')
            elif isinstance(node.ctx, ast.AugStore):
                self._visit('unreachable ;; AugStore')
            elif isinstance(node.ctx, ast.Param):
                self._visit('unreachable ;; Param')
        elif isinstance(node, ast.List):
            self._visit('unreachable ;; List')
        elif isinstance(node, ast.Tuple):
            self._visit('unreachable ;; Tuple')
        else:
            self._visit('unreachable ;; ' + str(type(node)))
