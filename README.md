# PyWASM
Python compiler and assembler for WebAssembly

## Installation

1. Install `requirements.txt` with pip3
2. Install [WABT](https://github.com/webassembly/wabt) (tested with 1724d710dd00b3155f8989d0cbd9558941d7ea35)
3. Configure and install `pywasm.service`

## Features

Just a simple project for fun. Supports most Python syntax, but only integer values. Standard library only includes a print function.
