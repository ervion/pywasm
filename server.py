#!/usr/bin/python3
import subprocess
import tempfile
import traceback

from bottle import get, post, run, static_file, request, response

from source2wat import Program


@get("/")
def index():
    return static_file('environment.html', ".")


@post("/wat")
def compile_wat():
    """ Compile Python to WebAssembly text format

    This action is actually safe and idempotent, but GET does not support body and long URLs were too unreliable.
    :return: assembly OR error text
    """
    try:
        return Program(request.body.read()).wat
    except Exception as _:
        response.status = 400
        return traceback.format_exc()


@post("/wasm")
def wasm():
    """ Assemble WebAssembly text format to WebAssembly binary format

    This action is actually safe and idempotent, but GET does not support body and long URLs were too unreliable.
    :return: binary OR error text
    """
    with tempfile.NamedTemporaryFile() as wat_file:  # wat2wasm seeks around, so pipe can't be used
        wat_file.write(request.body.read())
        wat_file.flush()

        try:
            p = subprocess.run(
                ["wast2wasm", "-o", "/dev/stdout", wat_file.name],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except FileNotFoundError as e:
            response.status = 500
            return e.strerror

        if p.returncode == 0:
            return p.stdout
        else:
            response.status = 400
            return p.stderr


run(host='localhost', port=8090)
